
$(document).ready(function() {
    //Menu Mobile-----------------
    var $menu = $("#mainMenu").clone();
    $menu.attr("id", "my-mobile-menu");
    $menu.mmenu({
        extensions: ["theme-white"]
    });
    //End Menu Mobile-----------------

    // Fixed Menu
    var section = $("#fix_header");
    var start = $(section).offset().top;
    $.event.add(window, "scroll", function() {
        var b = $(window).scrollTop();
        $(section).css("position", ((b) > start) ? "fixed" : "relative");
        $(section).css("top", ((b) > start) ? "0px" : "");
        $(section).css("width", ((b) > start) ? "100%" : "");
        $(section).css("z-index", ((b) > start) ? "999" : "");
        $(section).css("background", ((b) > start) ? "#fff" : "");
        $(section).css("box-shadow", ((b) > start) ? "0px 0px 10px -5px #a0a0a0" : "");
        if (b <= 30) {
            $(section).removeClass("scrollHeader")
        } else {
            $(section).addClass("scrollHeader")
        }
    })
    // End Fixed Menu

});
